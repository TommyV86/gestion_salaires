package entities;

import java.util.ArrayList;

public class Personnel {
	static ArrayList<Employe> employes = new ArrayList<>();
	
	public Personnel() {
		
	}
	
	public void add(Employe employe) {
		employes.add(employe);
	}
	
	public void displaySalary(ArrayList<Employe> employes) {
		for(Employe currentEmploye : employes) {
			System.out.println( "Nom : " + currentEmploye.getLastName()  +  ", salaire : " + currentEmploye.getSalary());
		}
	}
	
	public void displayMoyenSalary(ArrayList<Employe> employes) {
		Double somme = 0.0;
		Double moyenne = 0.0;
		for(Employe currentEmploye : employes) {
			somme = currentEmploye.getSalary() + somme;
		}
		moyenne = somme / employes.size();
		
		System.out.println("la moyenne des salaires est de : " + moyenne);
	}
	
	public ArrayList<Employe> getEmployes(){
		return employes;
	}
}
