package entities;

public abstract class Employe {

	private static Integer nextId = 0;
	private Integer id;
	private String lastName;
	private String firstName;
	private Integer age;
	private String date;
	private Double salary;
	private Double turnOver = 10000d;
	private boolean isRisk = false;
	
	public Employe() {
	}
	
	public Employe(String lastName, String firstName, Integer age, String date) {
		this.id = ++nextId;
		this.lastName = lastName;
		this.firstName = firstName;
		this.age = age;
		this.date = date;
	}
	
	public Employe(String lastName, String firstName, Integer age, String date, Boolean isRisk) {
		this.id = ++nextId;
		this.lastName = lastName;
		this.firstName = firstName;
		this.age = age;
		this.date = date;
		this.isRisk = isRisk;
	}

	public static Integer getNextId() {
		return nextId;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public static void setNextId(Integer nextId) {
		Employe.nextId = nextId;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public Double getTurnOver() {
		return turnOver;
	}

	public void setTurnOver(Double turnOver) {
		this.turnOver = turnOver;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public boolean isRisk() {
		return isRisk;
	}

	public void setRisk(boolean isRisk) {
		this.isRisk = isRisk;
	}

	public abstract void calculateSalary(Employe employe);
	
	
	@Override
	public String toString() {
	   return  "id : " + this.id +
			   "\n nom : " + this.lastName +
			   "\n prenom : " + this.firstName +
			   "\n age : " + this.age +
			   "\n date entree : " + this.date +
			   "\n chiffres affaires : " + this.turnOver;
	}
}
