package entities;

public class EmployeRepresentation extends Employe{

	public EmployeRepresentation() {}
	
	public EmployeRepresentation(String lastName, String firstName, Integer age, String date) {
		super(lastName, firstName, age, date);
	}

	@Override
	public void calculateSalary(Employe employe) {
		Double formule = ( 20.0 * 100.0 / super.getTurnOver() );
		employe.setSalary(formule * super.getTurnOver() + 800);
		System.out.println(employe.getSalary());
	}
	
	@Override
	public String toString() {
		return  " === Representateur === " +
				"\n " + super.toString() +
				"\n salaire : " + this.getSalary() +
				"\n========================" +
				"\n \n \n";
	}	
}
