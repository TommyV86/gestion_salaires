package entities;

public class EmployeProduction extends Employe {
	
	private Double unitProduct;
	private boolean isRisk = false;
	
	public EmployeProduction() {}
	
	public EmployeProduction(Double unitProduct, String lastName, String firstName, Integer age, String date, Boolean isRisk) {
		super(lastName, firstName, age, date);
		this.unitProduct = unitProduct;
		this.isRisk = isRisk;
	}
	
	public boolean isRisk() {
		return isRisk;
	}

	public void setRisk() {
		this.isRisk = true;
	}

	public EmployeProduction(Double unitProduct, String lastName, String firstName, Integer age, String date) {
		super(lastName, firstName, age, date);
		this.unitProduct = unitProduct;
	}
	
	public Double getUnitProduct() {
		return unitProduct;
	}

	public void setUnitProduct(Double unitProduct) {
		this.unitProduct = unitProduct;
	}

	@Override
	public void calculateSalary(Employe employe) {
		employe.setSalary(this.unitProduct * 5);
	}

	
	@Override
	public String toString() {
		return  "=== Ouvrier en production ===" + 
				"\n " + super.toString() + 
				"\n salaire : " + this.getSalary() + 
				"\n========================" + 
				"\n \n \n";
	}


}
