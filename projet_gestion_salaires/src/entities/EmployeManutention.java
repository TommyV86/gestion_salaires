package entities;

public class EmployeManutention extends Employe {
	
	public Double hoursWorked;
	
	public EmployeManutention() {}

	public EmployeManutention(Double hoursWorked, String lastName, String firstName, Integer age, String date, Boolean isRisk) {
		super(lastName, firstName, age, date, isRisk);
		this.hoursWorked = hoursWorked;
	}

	@Override
	public void calculateSalary(Employe employe) {
		employe.setSalary(this.hoursWorked * 65);
		
	}
	
	public Double getHoursWorked() {
		return hoursWorked;
	}

	public void setHoursWorked(Double hoursWorked) {
		this.hoursWorked = hoursWorked;
	}


	public Double calculSalary(Double hoursWorked) {
		return this.hoursWorked * 65;
	}
	

	@Override
	public String toString() {
		return 	"=== Manutentionnaire ===" +
				"\n " + super.toString() + 
				"\n heures de travail : " + this.getHoursWorked() +
				"\n salaire : " + this.getSalary() + 
				"\n prime de risque : " + (this.isRisk() == true ? "oui" : "non") +
				"\n========================" + 
				"\n \n \n";
	}

	

}
