package fr.afpa;

import java.util.Scanner;

import entities.Employe;
import entities.EmployeManutention;
import entities.EmployeProduction;
import entities.EmployeRepresentation;
import entities.EmployeVente;
import entities.Personnel;
import services.ServiceEmployeDatasIMP;
import services.ServiceEmployeIMP;
import views.AppView;

public class Main {
	
	private static Scanner sc = new Scanner(System.in);
	private static EmployeProduction employeProduction;
	private static ServiceEmployeIMP serviceEmployeIMP = new ServiceEmployeIMP();
	private static ServiceEmployeDatasIMP serviceEmployeDatasIMP = new ServiceEmployeDatasIMP();
	private static Personnel personnel = new Personnel();
	private static AppView appView = new AppView();
	private	static Integer choice;
	
	
	
	public static void main(String[] args) {
		
		serviceEmployeDatasIMP.createFile();
		appView.displayMessage("\nAppuyer sur entrée pour continuer ...");
		sc.nextLine();
		
		do {
			
			appView.displayMenu();
			choice = Integer.parseInt(sc.nextLine());
			
			switch (choice) {
			
			case 1:
				
				Employe employeVente = new EmployeVente();
				employeVente = serviceEmployeIMP.create(employeVente);
				employeVente.calculateSalary(employeVente);
				
				System.out.println(employeVente.toString());
				personnel.add(employeVente);
				break;
				
			case 2:

				employeProduction = new EmployeProduction(50.0, "toto", "tata", 12, "444");
				employeProduction.calculateSalary(employeProduction);
				System.out.println(employeProduction.toString());	
				System.out.println("salaire AVANT prime de risque : " + employeProduction.getSalary());
				employeProduction.setRisk();
				if(employeProduction.isRisk() == true) {
					serviceEmployeIMP.riskPrime(employeProduction);
					System.out.println("salaire APRES prime de risque : " + employeProduction.getSalary()); 
				}
				
				personnel.add(employeProduction);
				break;
				
			case 3:

				Employe employeRepresentation = new EmployeRepresentation();
				employeRepresentation = serviceEmployeIMP.create(employeRepresentation);
				employeRepresentation.calculateSalary(employeRepresentation);
				
				System.out.println(employeRepresentation.toString());
				personnel.add(employeRepresentation);
				break;
				
			case 4:

				Employe employeManutention = new EmployeManutention();
				employeManutention = serviceEmployeIMP.create(employeManutention);
				employeManutention.calculateSalary(employeManutention);
				serviceEmployeIMP.isGetPrime(employeManutention, serviceEmployeIMP);
				
				System.out.println(employeManutention.toString());
				personnel.add(employeManutention);
				break;
				
			case 5:

				personnel.displaySalary(personnel.getEmployes());
				break;
				
			case 6:
				personnel.displayMoyenSalary(personnel.getEmployes());
				break;
				
			case 0:
				
				appView.displayMessage(" *** au revoir ! ***");
				serviceEmployeDatasIMP.saveData(personnel.getEmployes());
				break;

			default:
				break;
			}
			
		} while(choice != 0);

	}

}
