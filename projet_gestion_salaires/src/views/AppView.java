package views;

public class AppView {

	public void displayMenu() {
		System.out.println("****** salaire employés *******");
		System.out.println();
		System.out.println("\t [1] - salaire employé vente  ");
		System.out.println("\t [2] - salaire employé production ");
		System.out.println("\t [3] - salaire employé représentation ");
		System.out.println("\t [4] - salaire employé manutention ");	
		System.out.println("\t [5] - afficher les salaires ");	
		System.out.println("\t [6] - afficher la moyenne des salaires ");	
		System.out.println("\t [0] - Fermer l'application ");
		System.out.println();
		System.out.println("**************************************");
	}
	
	public void displayMessage(String message) {
		System.out.println(message);
	}
}
