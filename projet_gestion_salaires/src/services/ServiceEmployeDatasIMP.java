package services;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import entities.Employe;

public class ServiceEmployeDatasIMP implements IserviceEmployeDatas {

	@Override
	public void createFile() {
		try {
			String pathEmployes = "../projet_gestion_salaires/src/fr/afpa/datas/employes.csv";

			File employes = new File(pathEmployes);

			if (employes.createNewFile()) {
				System.out.println();
				System.out.println();
				System.out.println("Fichier de la liste des employés  crée : " + employes.getName());
			} else {
				System.out.println();
				System.out.println();
				System.out.println(
						"createFiles : Pas de creation du fichier " + employes.getName() + " car il est deja existant");
			}

		} catch (Exception e) {
			System.out.println("Erreur de processus");
			e.printStackTrace();
		}

	}

	@Override
	public void saveData(ArrayList<Employe> employes) {
		try {
			String path = "../projet_gestion_salaires/src/fr/afpa/datas/employes.csv";
			PrintWriter myWriter = new PrintWriter(path);

			for (Employe currentEmploye : employes) {
				myWriter.print(currentEmploye.toString());
				myWriter.write("\n");
			}

			myWriter.close();
			System.out.println();
			System.out.println("Sauvegarde du fichier des employes effectuee.\n");
		} catch (Exception e) {
			System.out.println("Erreur de processus");
			e.printStackTrace();
		}

	}

}
