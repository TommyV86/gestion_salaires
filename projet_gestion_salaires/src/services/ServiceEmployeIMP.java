package services;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import entities.Employe;
import entities.EmployeManutention;
import entities.EmployeProduction;
import entities.EmployeRepresentation;
import entities.EmployeVente;

public class ServiceEmployeIMP implements IserviceEmploye {
	
	static Scanner sc = new Scanner(System.in);

	@Override
	public void riskPrime(Employe employe) {
		employe.setSalary(employe.getSalary() + 200);

	}

	@Override
	public void isGetPrime(Employe employe, IserviceEmploye iServiceEmp) {
		if (employe.isRisk() == true) {
			iServiceEmp.riskPrime(employe);
			System.out.println("salaire avec prime de risque : " + employe.getSalary() + "\n");
		} else {
			System.out.println("salaire sans prime de risque : " + employe.getSalary() + "\n");
		}
	}

	@Override
	public Employe create(Employe employe) {
		
		DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd MM uuuu");
		
		System.out.println("==== Nouvel employé ====");
		System.out.println(" nom : ");
		String lastName = sc.nextLine();
		
		System.out.println(" prenom : ");
		String firstName = sc.nextLine();
		
		System.out.println(" age : ");
		Integer age = Integer.parseInt( sc.nextLine());
		
		System.out.println(" date d'entrée : ");
		
		System.out.println(" jour : ");
		int jour = Integer.parseInt(sc.nextLine());	
		System.out.println(" mois : ");
		int mois = Integer.parseInt(sc.nextLine()); 
		System.out.println(" année : ");
		int annee = Integer.parseInt(sc.nextLine());
		
		LocalDate entryDate = LocalDate.of(annee, mois, jour);
		
		if( employe instanceof EmployeVente) {
			
			employe = new EmployeVente(lastName, firstName, age, entryDate.format(dateFormat));
			
		} else if( employe instanceof EmployeManutention) {
			
			System.out.println(" heures de travail : ");
			Double hoursWork = Double.parseDouble(sc.nextLine());
			
			System.out.println(" prime de risque : [O]ui / entrée pour non");
			Boolean isRisk = sc.nextLine().equalsIgnoreCase("O") ? true : false;
			
			employe = new EmployeManutention(hoursWork ,lastName, firstName, age, entryDate.format(dateFormat), isRisk);
			
		} else if( employe instanceof EmployeRepresentation ) {
			
			employe = new EmployeRepresentation(lastName, firstName, age, entryDate.format(dateFormat));
		
		} else if( employe instanceof EmployeProduction ) {
			
			System.out.println(" heures de travail : ");
			Double hoursWork = Double.parseDouble(sc.nextLine());
			
			System.out.println(" prime de risque : ");
			Boolean isRisk = sc.nextLine().equalsIgnoreCase("O") ? true : false;
			
			employe = new EmployeProduction(hoursWork, lastName, firstName, age, entryDate.format(dateFormat), isRisk);
		}
		
		return employe;	
	}
}
