package services;

import entities.Employe;

public interface IserviceEmploye {
	
	public void riskPrime(Employe employe);
	
	public void isGetPrime(Employe employe, IserviceEmploye iServiceEmp);

	public Employe create(Employe employe);
}
